from setuptools import setup, find_packages

import Vkbot

setup(
    name='Vkbot',
    version=Vkbot.__version__,
    description='General Bot for VK Groups',
    author='GeneralFIve',
    author_email='BrigadaFive@yandex.ru',
    url='https://vk.com/endecoder/',
    packages=find_packages(),
    install_requires=[
            'vk_api==11.3.0',
            'Pillow==5.3.0',
            'qrcode==6.0'
        ],
    entry_points={
            'console_scripts': [
                    'run = Vkbot.core:startbot'
                ]
        }
)