"""
    This module serves as encryption and decryption.
"""

from enum import Enum

class CipherModes(Enum):
    """
        Class Cipher Modes.
        Contains a list of available of cipher modes.
    """
    ENCRYPT = 0,
    DECRYPT = 1

class Caesar():
    """
        Class Caesar.
        Used to convert the text with Caesar's cipher.
    """
    _mode = CipherModes.ENCRYPT

    def setMode(self, mode):
        """ Set current cipher mode """
        self._mode = mode

    def getKey(self):
        """ Return key """
        return 3

    def cipher(self, text):
        """ Do cipher by selected mode and return result text """
        if self._mode == CipherModes.ENCRYPT: return self.encrypt(text)
        else: return self.decrypt(text)

    def encrypt(self, text):
        """ Encrypt method """
        newtext = ""
        for idx, i in enumerate(text):
            if i.isalpha(): newtext += shiftAlpha(i, self.getKey())
            else: newtext += i
        return newtext

    def decrypt(self, text):
        """ Decrypt method """
        newtext = ""
        for idx, i in enumerate(text):
            if i.isalpha(): newtext += shiftAlpha(i, -self.getKey())
            else: newtext += i
        return newtext

class Viginer():
    _mode = CipherModes.ENCRYPT
    _key = None

    def setMode(self, mode):
        """ Set current cipher mode """
        self._mode = mode

    def getKey(self):
        """ Return key """
        return self._key

    def cipher(self, text):
        """ Do cipher by selected mode and return result text """
        if self._mode == CipherModes.ENCRYPT: return self.encrypt(text)
        else: return self.decrypt(text)

    def setKey(self, key):
        """ Set key for encrypt/decrypt """
        self.key = key

    def encrypt(self, text):
        """ Encrypt method """
        newtext = ""
        for idx, i in enumerate(text):
            if i.isalpha(): newtext += shiftAlpha(i, indexAlpha(self.key[idx % len(self.key)]))
            else: newtext += i
        return newtext

    def decrypt(self, text):
        """ Decrypt method """
        newtext = ""
        for idx, i in enumerate(text):
            if i.isalpha(): newtext += shiftAlpha(i, -indexAlpha(self.key[idx % len(self.key)]))
            else: newtext += i
        return newtext

def indexAlpha(letter):
    """ Returns the letter number in the alphabet. """
    let = ord(letter)
    if (let >= ord('A') and let <= ord('Z')):
        let -= ord('A')
    elif (let >= ord('А') and let <= ord('Я')):
        let -= ord('А')
    elif (let >= ord('a') and let <= ord('z')):
        let -= ord('a')
    elif (let >= ord('а') and let <= ord('я')):
        let -= ord('а')
    else:
        let = -1
    return let + 1

def shiftCycle(a, b, N, count):
    """ Makes a cyclic shift in range from A to B on COUNT for number N and return new N. """
    N += count
    if N > b:
        N -= b - a + 1
    elif N < a:
        N += b - a + 1
    return N

def shiftAlpha(letter, count):
    """ Shift letter on count and return them. """
    let = ord(letter)
    if (let >= ord('A') and let <= ord('Z')):
        let = shiftCycle(ord('A'), ord('Z'), let, count)
    elif (let >= ord('А') and let <= ord('Я')):
        let = shiftCycle(ord('А'), ord('Я'), let, count)
    elif (let >= ord('a') and let <= ord('z')):
        let = shiftCycle(ord('a'), ord('z'), let, count)
    elif (let >= ord('а') and let <= ord('я')):
        let = shiftCycle(ord('а'), ord('я'), let, count)
    letter = chr(let)
    return letter