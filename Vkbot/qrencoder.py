"""
    This module serves as convert text to QR-code.
"""

import qrcode

class QREncoder():
    """
        Class QREncoder.
        Used for converting text to QR code.
    """

    def encode(text, savePath='qrCode.jpg'):
        """
            Convert text to QR-code and save QR-image save this.
            Return save path.
        """
        qrImg = qrcode.make(text)
        qrImg.save(savePath)
        return savePath