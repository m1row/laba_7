"""
   The main module of the bot.
"""

# -*- coding: utf-8 -*-
# ГРУППА ВКОНТАКТЕ: https://vk.com/club174154791

import os
from Vkbot import imgprocessor as ip, qrencoder as qr, botapi, cryptor as cv

def onMessageIncoming(user, message, attachments):
    """
       Processing incoming messages from the group
    """
    if user.updStep() == botapi.Stages.MAIN or message == 'Вернуться в начало':
        user.updStep(botapi.Stages.TYPE)
        user.write('Здравствуйте, ' + user.getName() + '.\nВас приветствует General Бот 2018.\n\nВыберите операцию, которую хотите выполнить, при помощи нажатия на соответствующую кнопку или вводом цифры из списка:\n' + botapi.Answers.TYPE.getList('&#128313; '), keyboard=botapi.Answers.TYPE.getKeyboard())

    elif user.updStep() == botapi.Stages.TYPE:
        if message.isnumeric(): actionid = botapi.Answers.TYPE.getActionId(id=int(message))
        else: actionid = botapi.Answers.TYPE.getActionId(text=message)
        if actionid != None:
            if actionid in range(1,3):
                user.updStep(botapi.Stages.CIPH_TYPE)
                if actionid == 1: user.cryptor = cv.Caesar()
                else: user.cryptor = cv.Viginer()
                user.write('Вы выбрали "' + botapi.Answers.TYPE.buttons[actionid - 1].text + '".\nКакое действие потребуется выполнить, рядовой?\n\nВыберите режим при помощи кнопки или вводом цифры из списка:\n' + botapi.Answers.CIPH.getList('&#128313; '), keyboard=botapi.Answers.CIPH.getKeyboard())
            elif actionid == 3:
                user.updStep(botapi.Stages.QRCODE)
                user.write('Вы выбрали операцию по преобразованию текста в QR-код.\nВведите текст, который хотите преобразовать: ', keyboard=botapi.Answers.BACK.getKeyboard())
            elif actionid == 4:
                user.updStep(botapi.Stages.IMGBLUR)
                user.write('Вы выбрали операцию размытию фотографий.\nЗагрузите фотографии и отправьте их для наложения эффекта размытия.', keyboard=botapi.Answers.BACK.getKeyboard())
        else:
            user.write('&#10071; Произошла ошибка! Такого действия не существует.\nПовторите попытку нажатием на соответствующую кнопку или введите нужную цифру из списка:\n' + botapi.Answers.TYPE.getList('&#128312; '), keyboard=botapi.Answers.TYPE.getKeyboard())

    elif user.updStep() == botapi.Stages.CIPH_TYPE:
        if message.isnumeric(): actionid = botapi.Answers.CIPH.getActionId(id=int(message))
        else: actionid = botapi.Answers.CIPH.getActionId(text=message)
        if actionid != None:
            if actionid == 1: user.cryptor.setMode(cv.CipherModes.ENCRYPT)
            else: user.cryptor.setMode(cv.CipherModes.DECRYPT)
            if user.cryptor.getKey() != None:
                user.updStep(botapi.Stages.CIPH_MSG)
                user.write('Вы выбрали режим "' + botapi.Answers.CIPH.buttons[actionid - 1].text + '".\n&#128196; Введите текст, который необходимо преобразовать:', keyboard=botapi.Answers.BACK.getKeyboard())
            else:
                user.updStep(botapi.Stages.CIPH_KEY)
                user.write('Вы выбрали режим "' + botapi.Answers.CIPH.buttons[actionid - 1].text + '".\n&#128272; Введите ключ, который будет использоваться для преобразования текста:', keyboard=botapi.Answers.BACK.getKeyboard())
        else:
            user.write('&#10071; Произошла ошибка! Такого действия не существует.\nПовторите попытку нажатием на соответствующую кнопку или введите нужную цифру из списка:\n' + botapi.Answers.CIPH.getList('&#128312; '), keyboard=botapi.Answers.CIPH.getKeyboard())

    elif user.updStep() == botapi.Stages.CIPH_KEY:
        if len(message) > 0:
            user.updStep(botapi.Stages.CIPH_MSG)
            user.cryptor.setKey(message)
            user.write('Вы успешно установили ключ.\n&#128196; Теперь введите, который необходимо преобразовать:')
        else:
            user.write('&#10071; Произошла ошибка! Ваше сообщение не содержит текста.\nПовторите попытку ввода ключа:', keyboard=botapi.Answers.BACK.getKeyboard())

    elif user.updStep() == botapi.Stages.CIPH_MSG:
        if len(message) > 0:
            user.updStep(botapi.Stages.MAIN)
            user.write('&#10004; Текст успешно преобразован. Результат:\n' + user.cryptor.cipher(message), keyboard=botapi.Answers.BACK.getKeyboard())
        else:
            user.write('&#10071; Произошла ошибка! Ваше сообщение не содержит текста.\nПовторите попытку ввода текста:', keyboard=botapi.Answers.BACK.getKeyboard())

    elif user.updStep() == botapi.Stages.QRCODE:
        if len(message) > 0:
            user.updStep(botapi.Stages.MAIN)
            qrcode = qr.QREncoder.encode(message)
            user.write('&#10004; Текст успешно преобразован в QR-код.', attachment=qrcode, keyboard=botapi.Answers.BACK.getKeyboard())
            os.remove(qrcode)
        else:
            user.write('&#10071; Произошла ошибка! Ваше сообщение не содержит текста.\nПовторите попытку ввода текста:', keyboard=botapi.Answers.BACK.getKeyboard())

    elif user.updStep() == botapi.Stages.IMGBLUR:
        photos = attachments.getPhotos()
        if photos is not None:
            user.updStep(botapi.Stages.MAIN)
            for p in photos:
                ip.ImageProcessor.addBlur(p)
            user.write('&#10004; Фотографии успешно преобразованы.', attachment=photos, keyboard=botapi.Answers.BACK.getKeyboard())
            for p in photos:
                os.remove(p)
        else:
            user.write('&#10071; Произошла ошибка! Прикрепите фотографии к вашему сообщению.', keyboard=botapi.Answers.BACK.getKeyboard())

def startbot():
    """
       Start listen VK group
    """
    bot = botapi.botVk();
    bot.listen(onMessageIncoming);

#if __name__ == '__main__':
#    main()
