"""
    This module serves as blur images.
"""

from PIL import Image, ImageFilter

class ImageProcessor():
    """
        Class ImageProcessor.
        Used for working with images.
    """

    def addBlur(imgPath, savePath=None):
        """
            Add blur on image and save this.
            Return save path.
        """
        if savePath is None: savePath = imgPath
        img = Image.open(imgPath)
        img = img.filter(ImageFilter.BoxBlur(15))
        img.save(savePath)
        img.close()
        return savePath