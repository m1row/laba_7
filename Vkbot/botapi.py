"""
    The module contains functions for managing VK events.
"""

from enum import Enum

import vk_api
import requests, time
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.keyboard import VkKeyboard, VkKeyboardColor

_vk_session = vk_api.VkApi(token='901cbe12be8d322900271d4180834eb7b0845f1e16d1172e2f22f7dcd698cde48395fff524bf1005ed1d6')
_vk = _vk_session.get_api()

class Colors(Enum):
    """
        Class Colors.
        Contains a list of available colors for buttons.
    """
    WHITE = VkKeyboardColor.DEFAULT
    GREEN = VkKeyboardColor.POSITIVE
    RED = VkKeyboardColor.NEGATIVE
    BLUE = VkKeyboardColor.PRIMARY

class Stages(Enum):
    """
        Class Stages.
        List of available bot states for a user to process incoming data.
    """
    MAIN = 0
    TYPE = 1
    CIPH_TYPE = 2
    CIPH_KEY = 3
    CIPH_MSG = 4
    QRCODE = 5
    IMGBLUR = 6

class Button():
    """
        Class Button.
        For initialize button with params.

        The constructor takes a text of button, color, wrether to add line and special param for hide text in list.
    """
    def __init__(self, text, color=Colors.BLUE, newLine=False, hideTxt=False):
        self.text = text
        self.color = color
        self.newLine = newLine
        self.hideTxt = hideTxt

class Keyboard():
    """
        Class Keyboard.
        For initialize VK Keyboard.

        The constructor takes a tuple of buttons.
    """
    def __init__(self, *button):
        if not type(button) is tuple: self.buttons = (button, )
        else: self.buttons = button
    def getActionId(self, id=None, text=None):
        """
            Return id of button in list by number or text.
            If buttons is not exist it returns None.
        """
        for index, i in enumerate(self.buttons, start=1):
            if id == index or text == i.text:
                return index
        return None
    def getList(self, startLine=''):
        """
            Return list of avaliable buttons by text.
        """
        butlist = ''
        for index, i in enumerate(self.buttons, start=1):
            if not i.hideTxt:
                butlist += startLine + str(index) + ') ' + i.text + '\n'
        return butlist
    def getKeyboard(self):
        """
            Return list of avaliable buttons by VK Keyboard.
        """
        keyboard = VkKeyboard(one_time=True)
        for i in self.buttons:
            keyboard.add_button(i.text, i.color.value)
            if i.newLine: keyboard.add_line()
        return keyboard.get_keyboard()

class Answers():
    """
        Class Answers.
        Contains pre-made sets of buttons for quick reference.
    """
    TYPE = Keyboard(Button('Шифр Цезаря'), Button('Шифр Вижинера', newLine=True), Button('Сгенерировать QR-код', newLine=True),
            Button('Загрузить и размыть фотографии'))
    CIPH = Keyboard(Button('Шифрование', color=Colors.GREEN), Button('Дешифрование', color=Colors.RED))
    BACK = Keyboard(Button('Вернуться в начало', color=Colors.WHITE, hideTxt=True))

class User():
    """
        Class User.
        Used to work with the user. It stores data such as the current step, user number, and other.

        The constructor take a VK user id.
    """
    _step = Stages.MAIN

    def __init__(self, user_id):
        self._user_id = user_id
        self.msgid = 0

    def getId(self):
        """
            Return user id
        """
        return self._user_id

    def updStep(self, step=None):
        """
            Set new user step if it was specified and return current step
        """
        if step != None: self._step = step
        return self._step

    def getName(self):
        """
            Return user first name
        """
        return _vk.users.get(user_ids=self._user_id,name_case='nom')[0]['first_name']

    def write(self, message=' ', attachment=None, keyboard=None):
        """
            Write message to user with attachments and keyboard (if it was specified)
        """
        self.msgid += 1
        if attachment != None:
            upload = vk_api.VkUpload(_vk_session)
            uploaded_photos = upload.photo_messages(attachment)
            attachment = []
            for photo in uploaded_photos:
                attachment.append('photo{}_{}'.format(photo['owner_id'], photo['id']))
            attachment = ','.join(attachment)

        _vk.messages.send(user_id=self._user_id, message=message, attachment=attachment, keyboard=keyboard, random_id=( time.time()*10**5 ))

class attachmentsMaster():
    """
        Class attachmentsMaster.
        Now it needed for parse and download photos from attachments.

        The constructor takes VK Event.
    """
    def __init__(self, event):
        self.peer_id = event.peer_id
        self.user_id = event.user_id
        self.attachments = event.attachments

    def get_img_size(self, sizes, size_type):
        """
           Return size if it exist in size_type list
        """
        for size in sizes:
            if size['type'] == size_type:
                return size

    def parse_json_photos(self, attachments):
        """
           Return list of urls photos from attachments
        """
        urls = dict()
        for attach in attachments:
            photo_id = attach['photo']['id']
            size = None
            for size_type in ['w', 'y', 'z', 'x', 'm', 's']:
                size = self.get_img_size(attach['photo']['sizes'], size_type)
                if size is not None: break
            url = size['url']
            urls[photo_id] = url
        return urls

    def download(self, fileLink, savePath):
        """
           Download file by link and return save path
        """
        with open(savePath, 'wb') as handle:
            response = requests.get(fileLink, stream=True)
            if not response.ok:
                print(response)
            for block in response.iter_content(1024):
                if not block: break
                handle.write(block)
        return savePath

    def getPhotos(self):
        """
           Download photos from current attachment and return list with paths to them.
        """
        if self.attachments == {}: return None

        photos_id = []
        k = list(self.attachments.keys())
        for item_type, item_value in self.attachments.items():
            if item_value == "photo":
                photos_id.append(self.attachments[k[k.index(item_type) + 1]])
        if photos_id == []: return None

        jsonout = _vk.messages.getHistory(user_id=self.peer_id,count=1)['items'][0]['attachments']
        urls_lst = self.parse_json_photos(jsonout)

        photos_id = []
        for photo_id in urls_lst:
            photos_id.append(self.download(urls_lst[photo_id], str(photo_id) + '.jpg'))
        return photos_id

class botVk():
    """
       Class botVk.
       Needed for listen events from VK and call function in main code.
    """
    users = dict()

    def __init__(self):
        self.longpoll = VkLongPoll(_vk_session)

    def listen(self, listener):
        """
           Listen data from VK and send this to listener function.
        """
        for event in self.longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW:
                if event.to_me:
                    if not event.user_id in self.users.keys():
                        self.users[event.user_id] = User(event.user_id)
                    listener(self.users[event.user_id], event.text, attachmentsMaster(event))